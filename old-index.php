<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>I&amp;M Millioni Mingi-KSHS 3 MILLION TO BE WON</title>
<link rel="stylesheet" type="text/css" href="css/layout.css" />
<script type="text/javascript" src="//use.typekit.net/nuy7iyc.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40443528-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<?php
  //submit form and save to db
  $data = (object) $_REQUEST;
  global $wpdb;
  if(isset($data->submit)):
    include '../wp-load.php';

    // immilioni@imbank.co.ke, sales@imbank.com
    $emailbody = '<ul style="list-style-type:none;">';
    $emailbody .= '<li><b>Account Type:</b> '.$data->account.'</li>';
    $emailbody .= '<li><b>Names:</b> '.$data->names.'</li>';
    $emailbody .= '<li><b>Email:</b> '.$data->email.'</li>';
    $emailbody .= '<li><b>Phone:</b> '.$data->phone.'</li>';
    $emailbody .= '<li><b>Location:</b> '.$data->location.'</li>';
    $emailbody .= '</ul>';

    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

     // More headers
     $headers .= 'From: '.$data->from_email;
     $sent = mail("sales@imbank.com,sales@imbank.co.ke", "Milioni Mingi Promotion", $emailbody,$headers);

     //auto-responder
    $headers_2 = "MIME-Version: 1.0" . "\r\n";
    $headers_2 .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
    $headers_2 .= 'From: I&M Bank <sales@imbank.co.ke>';
    $message_2 = 'Thank you very much for your email. A member of our team will respond to your query shortly.<br /><br />I&M Bank.';
    mail($data->email, 'Thank You', $message_2, $headers_2);

    //enter submission in database
    $date = date('Y-m-d H:i:s');
    $query = "INSERT INTO `sub_mail_queue` VALUES (NULL, '$data->email', 'sales@imbank.com', 'Milioni Mingi Submission', '$emailbody', 3, 0, 0, '$date', NULL, '0000-00-00 00:00:00', NULL, 1, 0)";
    $wpdb->query($query);
    $msg = 'Thank you for your interest.</br>We will contact you shortly.';
  endif;
?>
<body>
<style type="text/css">
#milioniForm label.error.error{
	color: rgb(252, 84, 84);
	font-size: 14px;
}
</style>
    <div class="topCell">
    	<div class="wrapper">
        	<div class="logo">
          		  <a href="index.php"><img src="images/im-logo.png" alt="I&quot;M Bank Limited" /></a>
            </div>
            <div class="topwidth">
                <div class="campaignTitle">
                	<img src="images/compaignName.png" />
                    <img src="images/happyGuy.png" />
                </div>
                <div class="happyGuy">
                		<h1 class="introTitle">ENTER NOW</h1>
                    <p class="formIntro">Fill in this form for your chance to win with I&M Milion Mingi.</p>
                  <div class="leftForm">
                    <?php if($msg): ?>
                    <div class="success" id="message"><?php echo $msg; ?></div>
                    <input name="submit" type="submit" class="backBtn" onclick="history.go(-1);" value="Go back" /><br />
                   <?php endif; 
                   
                   if(!isset($data->submit)):?>
                        <form method="get" action="?type=success" id="milioniForm">
                          <?php
                          $accounts = array('Choose a type of account to open','Sapphire Personal Account - 5 entry coupons','Bahati Personal Account - 3 entry coupons','Tayari Personal Account - 1 entry coupon',' Malaika Account - 4 entry coupons','Sapphire Business Account - 10 entry coupons','Alpha Business Account - 10 entry coupons','Biashara Business Account - 5 entry coupons','Savannah Business Account - 5 entry coupons');
                          ?>
                          <select name="account" id="account" class="select">
                          <?php foreach($accounts as $account): 
                                if($account == 'Choose a type of account to open'): ?>
                                    <option value=""><?php echo $account; ?></option>
                                <?php else: ?>
                                    <option value="<?php echo $account; ?>"><?php echo $account; ?></option>
                                <?php endif; ?>
                            <?php endforeach; ?>
                          </select>
                          <input name="names" type="text" value="Enter names" class="formIput" onfocus="if(this.value === this.defaultValue)this.value='';" onblur="if(this.value === '')this.value=this.defaultValue;" />
                          <input name="email" type="text" value="Enter email" class="formIput" onfocus="if(this.value === this.defaultValue)this.value='';" onblur="if(this.value === '')this.value=this.defaultValue;" />
                          <input name="phone" type="text" value="Enter phone no." class="formIput" onfocus="if(this.value === this.defaultValue)this.value='';" onblur="if(this.value === '')this.value=this.defaultValue;" />
                          <input name="location" type="text" value="Enter your location" class="formIput" onfocus="if(this.value === this.defaultValue)this.value='';" onblur="if(this.value === '')this.value=this.defaultValue;" />
                          <input name="submit" type="submit" class="sendBtn" />
                          <div class="clearBoth"></div>
                        </form>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="clearBoth"></div>
          </div>
        </div>
    </div>
    <div class="bottomCell">
    	<div class="wrapper">

        <!--Tabs starts here-->
        	<div id="tab-container" class="tab-container">
              <ul class='etabs'>
                <li class='tab'>
                	<div class="arrow"></div>
                    <div id="defaultArrow"></div>
                	<a href="#howToParticipate">How to Participate</a>
                </li>
                <li class='tab'>
               		 <div class="arrow"></div>
               		 <a href="#termsAndConditions">Terms and Conditions</a>
                </li>
                <li class='tab'>
                	<div class="arrow"></div>
                	<a href="#accountInformation">Account Information</a>
                </li>
                <div class="notTab"><a href="http://www.imbank.com/application-options/" target="_blank">Apply for an Account</a></div>
              </ul>
              <div class="clearBoth"></div>
              <div class="textSheet">
              		<div id="howToParticipate">
               			<h1>How to participate</h1>
                <p>
                	Open a Business or Personal account with any I&M Bank branch between  15th April - 15th July 2013, and  you could be one of the 3 lucky winners to WIN KSHS 1,000,000/- in 3 monthly draws.
Every account opened will receive entry coupon(s) depending on the type of account opened as follows:
                </p>
            <ul class="howTo">
                  <li>Sapphire Personal Account - 5 entry coupons</li>
                  <li>Malaika Account - 4 entry coupons</li>
                  <li>Bahati Personal Account - 3 entry coupons</li>
                  <li>Tayari Personal Account - 1 entry coupons</li>
                  <li>All current customers of I&amp;M Bank holding either of the above mentioned Personal or Business Transaction accounts automatically receive 1 ENTRY to the promotion.</li>
            </ul>
<p>For details and to enjoy the free home delivery service for account opening:
  <br />
  Call +254-020 -3221000, 0719-088000, 0732-100000,
SMS 0728-961169
Email: <a href="mailto:sales@imbank.co.ke">sales@imbank.co.ke</a></p>
             		</div>
              		<div id="termsAndConditions">
               			 <h1>Terms and Conditions</h1>
                 <ol class="terms">
					<li>
                        <span>1</span>
                        <p>All accounts opened shall be subject to the general  terms and conditions governing the opening and maintenance of such accounts and to the general tariffs of I&M Bank.</p>
                    </li>
                    <li>
                        <span>2</span>
                        <p>ATM Salary, Young Savers and Next Gen accounts are ineligible for this promotion.</p>
                    </li>
                    <li>
                        <span>3</span>
                        <p>All existing I&M Bank customers as of 15th April 2013 holding the participating accounts will automatically earn 1 entry which will be eligible for all the 3 draws held during the promotion.</p>
                    </li>
                    <li>
                        <span>4</span>
                        <p>Every account opened will receive entry coupon(s) depending on the type of account opened as follows:</p>
                    </li>
                  </ol>
			<ul class="subList">
            	<h2>Business Accounts</h2>
                <li>Sapphire Business Account - 10 entry coupons</li>
                <li>Biashara Business Account -5 entry coupons</li>
                <li>Alpha Business Account - 10 entry coupons</li>
                <li>Savannah Business Account - 5 entry coupons</li>
            </ul>
            <div class="clearBoth"></div>
            <ul class="subList">
            	<h2>Sapphire Personal Account - 5 entry coupons</h2>
                <li>Malaika Account - 4 entry coupons</li>
                <li>Bahati Personal Account - 3 entry coupons</li>
                <li>Tayari Personal Account - 1 entry coupons</li>
            </ul>
            <ol class="terms">
            <li>
                <span>5</span>
                <p>An account (new or existing) will be eligible for participation in all the subsequent draws provided the applicable minimum balance is maintained in all days during the promotion period.</p>
             </li>
              <li>
                <span>6</span>
                <p>New accounts will only be eligible for participation in the promotion provided they have submitted all relevant documentation pertaining to the account before the draw, the accounts have been duly opened and funded with at least the minimum balance.</p>
             </li>
             <li>
                <span>7</span>
                <p>Employees and close relatives of staff at I&M Bank, McCann and their associate companies are not eligible to participate in the promotion.</p>
             </li>
             <li>
                <span>8</span>
                <p>Home delivery service for account opening is available only within the city limits of Nairobi, Kisumu, Eldoret, Nakuru, Kisii and Mombasa.</p>
             </li>
             <li>
                <span>9</span>
                <p>The 3 draws will be held monthly at I&M Tower with the 1st draw taking place the week after the end of the each month, throughout the whole campaign period.</p>
             </li>
             <li>
                <span>10</span>
                <p>The monthly draws will be held in the presence of an official from the Betting Control & License Board and the winner will be announced in a local newspaper.</p>
             </li>
		</ol>
        <div class="clearBoth"></div>
              		</div>
             		<div id="accountInformation">
               			 <h1>BUSINESS ACCOUNTS</h1>
                         <ul class="infoList">
                            <h2>Sapphire Business Account-10 ENTRIES</h2>
                            <li>A high-end business transaction account, which is suitable for business and individuals who require a large number of banking transactions on a regular basis.<br />
</li>
<li>Minimum opening balance - Kshs.30,000.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Alpha Business Account - 10 ENTRIES</h2>
                            <li>A high-end flat fee business transaction account with a wide range of facilities such as free e-mailed transaction advice.
</li>
<li>No minimum opening balance.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Biashara Business Account - 5 ENTRIES</h2>
                            <li>A cost-effective business account which is suitable for business effecting a moderate number of banking transactions.
</li>
<li>Minimum opening balance - Kshs. 10, 000.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Savannah Business Account - 5 ENTRIES</h2>
                            <li>An affordable flat fee business transaction account tailored to suit the needs of a wide range of organizations.
</li>
<li>No minimum opening balance.</li>
                        </ul>

                       <h1>PERSONAL ACCOUNTS</h1>
                        <ul class="infoList">
                            <h2>Sapphire Personal Account - 5 ENTRIES</h2>
                            <li>A high-end personal transaction account with a host of facilities suitable for discerning individuals.
</li>
<li>Minimum opening balance - Kshs. 30, 000.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Malaika Account - 4 ENTRIES</h2>
                            <li>A personal transaction account exclusively designed to suit the financial requirement of women.
</li>
<li>Minimum opening balance - Kshs. 20, 000.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Bahati Personal Account - 3 ENTRIES</h2>
                            <li>A cost effective personal transaction account that fulfills all regular banking requirements.
</li>
<li>Minimum opening balance - Kshs. 10, 000.</li>
                        </ul>

                        <ul class="infoList">
                            <h2>Tayari Personal Account - 1 ENTRY</h2>
                            <li>A value account with a minimum balance of Kshs. 2, 000 that combines efficiency and affordability in order to meet your banking needs.
</li>
                        </ul>


              		</div>
              </div>
            </div>
        <!--Tabs ends here-->
        </div>
    </div>
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/hashchange.min.js"></script>
	<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
    <script type="text/javascript" src="js/allscript.js"></script>
    <script type="text/javascript">
//  $.noConflict();
    $(document).ready(function() 
    {
    jQuery.validator.addMethod("defaultInvalid", function(value, element) 
    {
        return !(element.value === element.defaultValue);
    });
    //2. Include custom validation in the rules for your element
    $("#milioniForm").validate(
    {
      rules: 
      {
       account: "required defaultInvalid",
       names: "required defaultInvalid",
       email: "required defaultInvalid",
       email: "email",
       phone: "required defaultInvalid",
       location: "required defaultInvalid"
      },
      messages: 
      {
       account: "Please select your account of interest",
       names: "Please input your name",
       email: "Please input your email",
       email: "Please input a valid email",
       phone: "Please input your phone",
       location: "Please input your location"
      }
    });
   });

</script>

</body>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35305586-1']);
  _gaq.push(['_setDomainName', '176.58.105.22.']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</html>
